Proceso ordenamiento_burbuja
	//definir variables
	Definir a,i,j,aux Como Entero;
	Dimension a[3];
	a[1]=109;
	a[2]=50;
	a[3]=20;
	//mostrar el arreglo
	Para i=1 Hasta 3 Con Paso 1 Hacer
		Escribir Sin Saltar a[i], " ";
	FinPara
	Escribir " Vector original ";
	//procedimiento de ordenamiento burbuja
	Para i=2 Hasta 3 Con Paso 1 Hacer
		
		Para j=1 Hasta 2 Con Paso 1 Hacer
			Si a[j] > a[j+1] Entonces
				aux=a[j];
				a[j]= a[j+1];
				a[j+1]=aux;
			SiNo
				
			FinSi
		FinPara
	FinPara
	//imprimir el vector ordenado
	Para i=1 Hasta 3 Con Paso 1 Hacer
		Escribir Sin Saltar a[i], " ";
	FinPara
	Escribir " Vector ordenado ";
FinProceso
