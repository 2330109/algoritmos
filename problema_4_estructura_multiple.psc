Proceso sin_titulo
	Definir x Como Entero;
	Escribir "Bienvenido a la tienda oficial de los uniformes de la upv.";
	Escribir "Seleccione la talla de la playera que desea.";
	Escribir "(1)Grande, (2)Mediana, (3)Chica, (4)Extra grande, (5)Extra chica.";
	Leer x;
	Segun x Hacer
		1:
			Escribir "Haz escogido la grande, son 325 pesos.";
		2:
			Escribir "Haz escogido la mediana, son 300 pesos.";
		3:
			Escribir "Haz escogido la chica, son 275 pesos.";
		4:
			Escribir "Haz escogido extra grande, son 350 pesos.";
		5:
			Escribir "Haz escogido extra chica, son 250 pesos.";
		De Otro Modo:
			Escribir "Disculpe, no contamos con mas tallas.";
	FinSegun
	Si x>=1 y x<=5 Entonces
		Escribir "Gracias por su compra";
	FinSi
	
FinProceso
